--
-- packages/acs-subsite/sql/user-profiles-drop.sql
--
-- @author oumi@arsdigita.com
-- @creation-date 2000-02-02
-- @cvs-id $Id: user-profiles-drop.sql,v 1.5 2020-11-03 18:29:24 cvs Exp $
--

drop table user_profiles;
drop package user_profile;

begin
  acs_rel_type.drop_type('user_profile');
end;
/
show errors

