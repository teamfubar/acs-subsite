ad_page_contract {
    Makes /pvt/ redirect to /pvt/home

    @author michael@arsdigita.com
    @creation-date 30 May 2000
    @cvs-id $Id: index.tcl,v 1.5 2020-11-03 18:29:25 cvs Exp $
} {}

ad_returnredirect "home"

# Local variables:
#    mode: tcl
#    tcl-indent-level: 4
#    indent-tabs-mode: nil
# End:
