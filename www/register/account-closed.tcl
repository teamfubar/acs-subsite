ad_page_contract {
    Inform the user that his/her account is closed

    @cvs-id $Id: account-closed.tcl,v 1.6 2020-11-03 18:29:25 cvs Exp $
} {

}

set page_title [_ acs-subsite.Account_closed_title]

set context [list [_ "acs-kernel.common_Register"]]


# Local variables:
#    mode: tcl
#    tcl-indent-level: 4
#    indent-tabs-mode: nil
# End:
