ad_page_contract {

    Home page for OpenACS Object Type administration

    @author Yonatan Feldman (yon@arsdigita.com)
    @creation-date August 13, 2000
    @cvs-id $Id: index.tcl,v 1.6 2020-11-03 18:29:25 cvs Exp $

} {}

set doc(title) "Object Type Hierarchical Index"
set context [list "Object Type Hierarchical Index"]

set object_type_hierarchy [acs_object_type_hierarchy]

# Local variables:
#    mode: tcl
#    tcl-indent-level: 4
#    indent-tabs-mode: nil
# End:
